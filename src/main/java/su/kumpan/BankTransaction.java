package su.kumpan;

import java.time.LocalDateTime;
import java.util.Objects;

public class BankTransaction implements Transaction {
    private final LocalDateTime dateTime;
    private final String summary;
    private final Double amt;
    private final int id;

    private static int idBase = 0;

    public BankTransaction(LocalDateTime dateTime, String summary, double amt) {
        this.dateTime = dateTime;
        this.summary = summary;
        this.amt = Math.signum(amt) * amt;
        id = idBase++;
    }

    @Override
    public Double getAmount() {
        return amt;
    }

    @Override
    public LocalDateTime getTime() {
        return dateTime;
    }

    @Override
    public String getDescription() {
        return summary;
    }

    @Override
    public TxType getType() {
        return TxType.BANK;
    }

    @Override
    public String toString() {
        return getTime().toLocalDate() + " for " + getAmount()
                + (getDescription() != null ? " " + getDescription() : "");
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        BankTransaction that = (BankTransaction) o;
        return id == that.id;
    }

    @Override
    public int hashCode() {
        return Objects.hash(id);
    }
}
