package su.kumpan;

public enum IssueType {
    IRREGULAR_DUPLICATES("Несимметричные записи"),
    MISSING_IN_CK("Не занесено в КК"),
    EXTRA_IN_CK("Лишнее в КК");

    private final String description;

    IssueType(String description) {
        this.description = description;
    }

    public String description() {
        return description;
    }
}
