package su.kumpan.reporting;

import com.google.common.base.Joiner;
import su.kumpan.Account;
import su.kumpan.DeltaReport;
import su.kumpan.Issue;
import su.kumpan.Transaction;

import java.io.*;
import java.nio.charset.StandardCharsets;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.Map;

public class CsvReporter {
    private final Map<String, String> knownCats;
    DateTimeFormatter FILE_FORMAT = DateTimeFormatter.ofPattern("yyyyMMddHHmm");

    public CsvReporter(Map<String, String> knownCats) {
        this.knownCats = knownCats;
    }

    public void generate(Account account, DeltaReport deltaReport, LocalDateTime from, LocalDateTime to) throws IOException {
        if (deltaReport.getIssues().isEmpty()) return;

        File destination = new File("./reports/report_" + account + "_" + from.format(FILE_FORMAT) + "-" + to.format(FILE_FORMAT) + ".csv");
        Writer writer = new OutputStreamWriter(new FileOutputStream(destination), StandardCharsets.UTF_8);

        writer.append("\"").append(Joiner.on("\",\"").join("Дата", "Время", "Тип", "Общая сумма", "Источник", "Сумма", "Описание", "Категория")).append("\"\n");
        for (Issue issue : deltaReport.getIssues()) {
            for (Transaction transaction : issue.getTransactions()) {
                String reportString = '"' + Joiner.on("\",\"").join(
                        transaction.getTime().toLocalDate(),
                        transaction.getTime().toLocalTime(),
                        issue.getType().description(),
                        issue.sum(),
                        transaction.getType(),
                        transaction.getAmount(),
                        transaction.getDescription(),
                        knownCats.getOrDefault(transaction.getDescription(), "")
                ) + "\"\n";
                writer.append(reportString);
            }
        }

        writer.close();
    }
}
