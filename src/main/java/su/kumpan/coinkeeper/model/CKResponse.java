package su.kumpan.coinkeeper.model;

import com.fasterxml.jackson.annotation.JsonProperty;

import java.util.List;

public class CKResponse {
    private final List<CKTransaction> transactions;
    private final boolean hasMoreData;

    public CKResponse(
            @JsonProperty("transactions") List<CKTransaction> transactions,
            @JsonProperty("hasMoreData") boolean hasMoreData
            ) {
        this.transactions = transactions;
        this.hasMoreData = hasMoreData;
    }

    public List<CKTransaction> getTransactions() {
        return transactions;
    }

    public boolean isHasMoreData() {
        return hasMoreData;
    }
}
