package su.kumpan.coinkeeper.model;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.google.common.base.Joiner;
import su.kumpan.Account;
import su.kumpan.Transaction;
import su.kumpan.coinkeeper.CKStatementDownloader;

import java.time.LocalDateTime;
import java.util.List;
import java.util.Objects;

public class CKTransaction implements Transaction {

    /*
    {
      "id": "4ea5958c-8230-47c5-9b3e-d147bfbcfc05",
      "importedTransactionId": null,
      "userId": "5181de713599b011445d8e53",
      "dateTimestamp": 637381927370000000,
      "dateTimestampISO": "2020-10-13T13:32:17Z",
      "defaultAmount": 180000.0,
      "deleted": false,
      "createdTimestamp": 637381927510000000,
      "createdTimestampISO": "2020-10-13T13:32:31Z",
      "timestamp": 637381927577440000,
      "timestampISO": "2020-10-13T13:32:37Z",
      "sourceId": "82b312fa-02b3-4efe-88c8-f3d560fd1513",
      "sourceType": 2,
      "sourceAmount": 180000.0,
      "destinationId": "0835a846-f563-4ac4-96c5-d42483b7b5fc",
      "destinationType": 2,
      "destinationAmount": 180000.0,
      "tags": [],
      "comment": "",
      "debtPaymentAmount": 0.0,
      "debtorCreditor": null,
      "debtDeadLine": 0,
      "debtDeadLineISO": "0001-01-01T00:00:00Z",
      "debtPaymentDate": 0,
      "debtPaymentDateISO": "0001-01-01T00:00:00Z",
      "debtPaymentTransactionId": null,
      "duplicated": false,
      "processed": false,
      "isComplete": null,
      "repeatingParentId": null,
      "counter": 0
    },
     */

    private final String sourceId;
    private final String destinationId;
    private final LocalDateTime dateTimestampISO;
    private final Double sourceAmount;
    private final String comment;
    private final List<String> tags;
    private final String id;

    public CKTransaction(
            @JsonProperty("destinationId") String destinationId,
            @JsonProperty("sourceId") String sourceId,
            @JsonProperty("dateTimestampISO") String dateTimestampISO,
            @JsonProperty("sourceAmount") double sourceAmount,
            @JsonProperty("comment") String comment,
            @JsonProperty("tags") List<String> tags,
            @JsonProperty("id") String id
            ) {
        this.sourceId = sourceId;
        this.destinationId = destinationId;
        this.dateTimestampISO = LocalDateTime.parse(dateTimestampISO, CKStatementDownloader.DT_FORMAT);
        this.sourceAmount = Math.signum(sourceAmount) * sourceAmount;
        this.comment = comment;
        this.tags = tags;
        this.id = id;
    }

    public CKTransaction(CKTransaction other, Double amount) {
        this.sourceId = other.sourceId;
        this.destinationId = other.destinationId;
        this.dateTimestampISO = other.dateTimestampISO;
        this.sourceAmount = amount;
        this.comment = other.comment;
        this.tags = other.tags;
        this.id = other.id;
    }

    @Override
    public Double getAmount() {
        return sourceAmount;
    }

    @Override
    public LocalDateTime getTime() {
        return dateTimestampISO;
    }

    @Override
    public String getDescription() {
        return ((tags == null || tags.isEmpty()) ? "" : " [" + Joiner.on("][").join(tags) + "]" ) + comment;
    }

    public List<String> getTags() {
        return tags;
    }

    public String getSourceId() {
        return sourceId;
    }

    public Account getSourceAccount() {
        return Account.byCkId(sourceId);
    }

    public Account getDestinationAccount() {
        return Account.byCkId(destinationId);
    }

    @Override
    public TxType getType() {
        return TxType.CK;
    }

    @Override
    public String toString() {
        return getTime().toLocalDate() + " for " + getAmount()
                + (sourceId != null ? " from " + sourceId : "")
                + (destinationId != null ? " to " + destinationId : "")
                + (tags != null ? " " + tags : "")
                + (comment != null ? " " + comment : "");
    }

    public String getDestinationId() {
        return destinationId;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        CKTransaction that = (CKTransaction) o;
        return id.equals(that.id);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id);
    }
}
