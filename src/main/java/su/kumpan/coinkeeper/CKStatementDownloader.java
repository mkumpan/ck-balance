package su.kumpan.coinkeeper;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.google.common.collect.HashMultimap;
import com.google.common.collect.Multimap;
import su.kumpan.Account;
import su.kumpan.Transaction;
import su.kumpan.coinkeeper.model.CKResponse;
import su.kumpan.coinkeeper.model.CKTransaction;
import su.kumpan.util.JsonBodyHandler;

import java.io.FileReader;
import java.io.IOException;
import java.net.CookieManager;
import java.net.HttpCookie;
import java.net.URI;
import java.net.http.HttpClient;
import java.net.http.HttpRequest;
import java.net.http.HttpResponse;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.*;
import java.util.function.Supplier;
import java.util.regex.Pattern;
import java.util.stream.Collectors;

import static java.time.LocalDateTime.now;
import static java.time.temporal.ChronoField.NANO_OF_DAY;
import static java.time.temporal.ChronoUnit.MONTHS;

public class CKStatementDownloader {

    public static final int PAGE_SIZE = 50;
    public static final Pattern NUM_ONLY_RX = Pattern.compile("(\\d+\\w*\\+?)+");
    private final CookieManager COOKIE_HANDLER;
    public static final DateTimeFormatter DT_FORMAT = DateTimeFormatter.ofPattern("yyyy-MM-dd'T'HH:mm:ss[.SSS]'Z'");
    public static final String UID = "5181de713599b011445d8e53";
    private final HttpClient client;
    private String userCookie;
    private String authCookie;

    public CKStatementDownloader(String ckUser, String ckAuth) {
        COOKIE_HANDLER = new CookieManager();
        client = HttpClient.newBuilder().cookieHandler(COOKIE_HANDLER).build();

        setCredentials(ckUser, ckAuth);
    }

    private void setCredentials(String ckUser, String ckAuth) {
        userCookie = ckUser;
        authCookie = ckAuth;
        HttpCookie userCookie = new HttpCookie("_COINKEEPER", this.userCookie);
        HttpCookie cookie = new HttpCookie("__AUTH_cookie", this.authCookie);
        COOKIE_HANDLER.getCookieStore().add(URI.create("https://coinkeeper.me"), cookie);
    }

    public Set<Transaction> getStatement(Account account, LocalDateTime from, LocalDateTime to) throws IOException, InterruptedException {

        Set<Transaction> transactions = new HashSet<>();
        boolean hasMore;
        int page = 0;
        do {
            CKResponse response = doRequest(account, from, to, page++);
            hasMore = response.isHasMoreData();
            if (null == response.getTransactions()) break;
            transactions.addAll(filter(response));
        } while (hasMore);

        return transactions;
    }

    private List<CKTransaction> filter(CKResponse response) {
        List<CKTransaction> result = new ArrayList<>();

        handleSplit(response, result);
        handlePlus(response, result);

        return result;
    }

    private void handleSplit(CKResponse response, List<CKTransaction> result) {
        Multimap<Double, CKTransaction> cache = HashMultimap.create();

        response.getTransactions().forEach(it -> {
            String description = it.getDescription().replaceAll("[^+\\d]", "");
            if (description.contains("+") | description.isBlank()) {
                result.add(it);
                return;
            }

            cache.put(Double.parseDouble(description), it);
        });

        for (Double value : cache.keySet()) {
            Collection<CKTransaction> ckTransactions = cache.get(value);
            Double sum = ckTransactions.stream().mapToDouble(CKTransaction::getAmount).sum();

            if (sum.equals(value)) {
                System.out.println("Found split entry for value " + value + " from " + ckTransactions.size() + " entries.");
                result.add(new CKTransaction(ckTransactions.iterator().next(), value));
            } else {
                result.addAll(ckTransactions);
            }
        }
    }


    private void handlePlus(CKResponse response, List<CKTransaction> result) {
        response.getTransactions().forEach(it -> {
            if (it.getDescription().contains("+")) {
                String description = it.getDescription().replaceAll("[^+\\d]", "");
                List<CKTransaction> els = Arrays.stream(description.split("\\+")).map(
                        part -> new CKTransaction(it, Double.parseDouble(part))
                ).collect(Collectors.toList());

                System.out.println("Found join entry: \"" + it.getDescription() + "\", split into " + els.size());
                result.addAll(els);
            } else {
                result.add(it);
            }
        });
    }

    private CKResponse doRequest(Account account, LocalDateTime from, LocalDateTime to, int page) throws IOException, InterruptedException {
        Map<Object, Object> params = new HashMap<>();
        params.put("categoryIds", buildCats(account));
        params.put("period", buildPeriod(from, to));
        params.put("skip", page * PAGE_SIZE);
        params.put("tagIds", new String[0]);
        params.put("take", PAGE_SIZE);
        params.put("userId", UID);

        var objectMapper = new ObjectMapper();
        String requestBody = objectMapper
                .writeValueAsString(params);

        HttpRequest request = HttpRequest.newBuilder()
                .POST(HttpRequest.BodyPublishers.ofString(requestBody))
                .uri(URI.create("https://coinkeeper.me/api/transaction/get"))
                .header("Content-Type", "application/json")
                .header("Cookie", "_COINKEEPER=" + userCookie + ";__AUTH_cookie=" + authCookie)
                .build();

        HttpResponse<Supplier<CKResponse>> response = client.send(request, new JsonBodyHandler<>(CKResponse.class));

        return response.body().get();
    }

    private Object buildPeriod(LocalDateTime from, LocalDateTime to) {
        Map<Object, Object> map = new HashMap<>();
        map.put("from", from.format(DT_FORMAT));
        map.put("to", to.format(DT_FORMAT));

        return map;
    }

    private Object buildCats(Account account) {
        String[] cats = new String[1];
        cats[0] = account.getCkId();

        return cats;
    }


    public static void main(String[] args) throws IOException, InterruptedException {

        Properties properties = new Properties();
        properties.load(new FileReader("src/main/resources/app.properties"));


        LocalDateTime from = sod(now().minus(1, MONTHS));
        LocalDateTime to = now();

        CKStatementDownloader ckStatementDownloader = new CKStatementDownloader(
                properties.getProperty("ck.user"),
                properties.getProperty("ck.auth")
                );

        Set<Transaction> statement = ckStatementDownloader.getStatement(Account.PERSONAL, from, to);

    }

    private static LocalDateTime sod(LocalDateTime dateTime) {
        return dateTime.with(NANO_OF_DAY, 0);
    }
}
