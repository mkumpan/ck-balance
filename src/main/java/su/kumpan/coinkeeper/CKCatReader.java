package su.kumpan.coinkeeper;

import su.kumpan.*;
import su.kumpan.coinkeeper.model.CKTransaction;

import java.util.*;

import static su.kumpan.Transaction.TxType.CK;

public class CKCatReader {

    private Set<String> blacklist = new HashSet<>();

    public Map<String, String> retrieve(Collection<DeltaReport> report) {
        Map<String, String> catMap = new HashMap<>();

        for (DeltaReport deltaReport : report) {
            doRetrieve(deltaReport, catMap);
        }

        return catMap;
    }


    public void doRetrieve(DeltaReport report, Map<String, String> result) {

        for (Match match : report.getMatches()) {
            if (match.getTransactions().stream()
                    .filter(it -> it.getType() == CK)
                    .map(it -> (CKTransaction) it)
                    .allMatch(this::isInternal)) {
                continue;
            }
            String description = null;
            String destId = null;

            for (Transaction tx : match.getTransactions()) {
                if (tx instanceof BankTransaction) {
                    BankTransaction bankTx = (BankTransaction) tx;
                    description = bankTx.getDescription();
                }
                if (tx instanceof CKTransaction) {
                    CKTransaction ckTx = (CKTransaction) tx;
                    destId = ckTx.getDestinationId();
                }
            }

            if (blacklist.contains(description)) {
                continue;
            }

            String oldVal = result.put(description, destId);
            if (oldVal != null && !oldVal.equals(destId)) {
                blacklist.add(description);
                result.remove(destId);
            }
        }
    }

    private boolean isInternal(CKTransaction transaction) {
        Set<String> internalIds = new HashSet<>();

        for (Account account : Account.values()) {
            if (account.getCkId() != null) {
                internalIds.add(account.getCkId());
            }
        }

        return internalIds.contains(transaction.getSourceId()) && internalIds.contains(transaction.getDestinationId());
    }
}
