package su.kumpan;

public enum MatchType {
    EXACT,
    ROUGH,
    NEXT_DAY,
    NEXT_DAY_ROUGH
}
