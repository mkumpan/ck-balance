package su.kumpan;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.google.common.collect.ImmutableList;
import com.google.common.collect.ImmutableSet;
import com.google.common.collect.Sets;
import su.kumpan.coinkeeper.model.CKTransaction;

import java.time.LocalDate;
import java.util.*;
import java.util.stream.Collectors;

public class DeltaReport {
    @JsonIgnore
    private final Set<Transaction> unprocessedBankTxs;
    @JsonIgnore
    private final Set<Transaction> unprocessedCkTxs;

    private int totalOffset;
    private int totalAligned;

    private final Map<LocalDate, Set<Issue>> issuesMap = new HashMap<>();
    private final Map<LocalDate, Set<Match>> matchesMap = new HashMap<>();

    private final Set<LocalDate> coveredDays;

    public DeltaReport(Set<Transaction> unprocessedBankTxs, Set<Transaction> unprocessedCkTxs) {
        this.unprocessedBankTxs = new HashSet<>(unprocessedBankTxs);
        this.unprocessedCkTxs = new HashSet<>(filterNonBankAccts(unprocessedCkTxs));

        this.coveredDays = buildCoveredDays();
    }

    public void registerIssue(IssueType type, List<Transaction> transactions) {
        if (transactions.isEmpty()) return;
        Issue issue = new Issue(ImmutableList.copyOf(transactions), type);

        for (Transaction transaction : transactions) {
            issuesMap.computeIfAbsent(transaction.getTime().toLocalDate(), it -> new HashSet<>()).add(issue);
            totalOffset += issue.sum();
        }

        for (Transaction transaction : ImmutableList.copyOf(transactions)) {
            remove(transaction);
        }
    }

    public Set<Transaction> getCkForDay(LocalDate day) {
        return getForDay(day, unprocessedCkTxs);
    }

    public Set<Transaction> getBankForDay(LocalDate day) {
        return getForDay(day, unprocessedBankTxs);
    }

    private Set<Transaction> getForDay(LocalDate day, Set<Transaction> target) {
        return target.stream().filter(it -> it.getTime().toLocalDate().equals(day)).collect(Collectors.toUnmodifiableSet());
    }

    public Set<LocalDate> getCoveredDays() {
        return coveredDays;
    }

    private Set<LocalDate> buildCoveredDays() {
        ImmutableSet.Builder<LocalDate> builder = ImmutableSet.builder();
        unprocessedBankTxs.forEach(it -> builder.add(it.getTime().toLocalDate()));
        unprocessedCkTxs.forEach(it -> builder.add(it.getTime().toLocalDate()));

        return new HashSet<>(builder.build());
    }

    public void match(Transaction left, Transaction right, MatchType matchType) {
        Match match = new Match(left, right, matchType);

        matchesMap.computeIfAbsent(left.getTime().toLocalDate(), it -> new HashSet<>()).add(match);
        matchesMap.computeIfAbsent(right.getTime().toLocalDate(), it -> new HashSet<>()).add(match);

        totalAligned += match.sum();

        remove(left);
        remove(right);
    }

    private static Set<Transaction> alreadyRemoved = new HashSet<>();
    private void remove(Transaction tx) {
        boolean remove = (tx instanceof BankTransaction ? unprocessedBankTxs : unprocessedCkTxs).remove(tx);
        if (!remove) {
            boolean contains = alreadyRemoved.contains(tx);
            int a = 1;
        } else {
            alreadyRemoved.add(tx);
        }
    }

    /**
     * Filters out CK transactions that don't affect watched bank accounts.
     */
    private Set<Transaction> filterNonBankAccts(Set<Transaction> transactions) {
        return transactions.stream()
                .filter(it -> {
                    Account sourceId = ((CKTransaction) it).getSourceAccount();
                    Account destinationId = ((CKTransaction) it).getDestinationAccount();
                    return Account.bankAccounts().contains(sourceId) || Account.bankAccounts().contains(destinationId);
                })
                .collect(Collectors.toUnmodifiableSet());
    }

    public List<Transaction> getUnprocessedBankTxs() {
        return ImmutableList.copyOf(unprocessedBankTxs);
    }

    public List<Transaction> getUnprocessedCkTxs() {
        return ImmutableList.copyOf(unprocessedCkTxs);
    }

    public int totalOffset() {
        return totalOffset;
    }

    public int totalAligned() {
        return totalAligned;
    }

    @JsonIgnore
    public Set<Issue> getIssues() {
        return issuesMap.values().stream().reduce(Sets::union).orElse(new HashSet<>());
    }


    public Set<Transaction> getBankTxBy(LocalDate day, Double value) {
        return getBy(unprocessedBankTxs, day, value);
    }

    public Set<Transaction> getCkTxBy(LocalDate day, Double value) {
        return getBy(unprocessedCkTxs, day, value);
    }

    public Set<Transaction> getBy(Collection<Transaction> target, LocalDate day, Double value) {
        return target.stream().filter(it -> it.getTime().toLocalDate().equals(day) && it.getAmount().equals(value)).collect(Collectors.toSet());
    }

    @JsonIgnore
    public Set<Match> getMatches() {
        return matchesMap.values().stream().reduce(Sets::union).orElse(new HashSet<>());
    }

    public Map<LocalDate, Set<Issue>> getIssuesMap() {
        return issuesMap;
    }

    public Map<LocalDate, Set<Match>> getMatchesMap() {
        return matchesMap;
    }
}
