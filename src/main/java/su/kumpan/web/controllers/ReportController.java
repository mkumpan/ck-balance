package su.kumpan.web.controllers;

import org.springframework.format.annotation.DateTimeFormat;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import su.kumpan.Account;
import su.kumpan.DeltaReport;
import su.kumpan.LifeCycle;

import java.io.IOException;
import java.time.LocalDate;
import java.util.Map;

@RestController
public class ReportController {

    @GetMapping("reports")
    public Map<Account, DeltaReport> getReports(
            @DateTimeFormat(pattern = "dd-MM-yyyy") @RequestParam("from") LocalDate from,
            @DateTimeFormat(pattern = "dd-MM-yyyy") @RequestParam("to") LocalDate to) throws IOException, InterruptedException {
        Map<Account, DeltaReport> reports = new LifeCycle().run(from.atStartOfDay(), to.atTime(23, 59, 59));

        return reports;
    }
}
