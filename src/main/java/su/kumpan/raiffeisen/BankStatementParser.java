package su.kumpan.raiffeisen;

import org.apache.commons.io.FileUtils;
import su.kumpan.Account;
import su.kumpan.BankTransaction;
import su.kumpan.Transaction;

import java.io.File;
import java.io.IOException;
import java.nio.charset.Charset;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

public class BankStatementParser {
    public static final int DATE_IDX = 0;
    public static final int ACCT_SUMMARY_IDX = 1;
    public static final int ACCT_AMT_IDX = 5;

    public static final int CARD_SUMMARY_IDX = 2;
    public static final int CARD_AMT_IDX = 9;

    private static final DateTimeFormatter DT_FORMAT = DateTimeFormatter.ofPattern("dd.MM.yyyy HH:mm");

    public Set<Transaction> parse(File statement, Account account) throws IOException {
        Set<Transaction> result = new HashSet<>();

        List<String> expenseLines = FileUtils.readLines(statement, Charset.forName("windows-1251"));

        expenseLines.stream().skip(1).forEach( expenseLine -> {
            String[] split = expenseLine.split(";");

            LocalDateTime dateTime = LocalDateTime.parse(split[DATE_IDX], DT_FORMAT);
            String summary = split[account.useCard() ? CARD_SUMMARY_IDX : ACCT_SUMMARY_IDX];
            double amt = Double.parseDouble(split[account.useCard() ? CARD_AMT_IDX : ACCT_AMT_IDX].replace(" ", ""));

            result.add(new BankTransaction(dateTime, summary, amt));
        });

        return result;
    }


    public static void main(String[] args) throws IOException {
        Set<Transaction> expens = new BankStatementParser().parse(new File("./statements/statement_CREDIT100_202006140000-202010141300.csv"), Account.CREDIT100);
    }
}
