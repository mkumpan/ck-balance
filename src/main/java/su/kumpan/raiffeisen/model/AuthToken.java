package su.kumpan.raiffeisen.model;

import com.fasterxml.jackson.annotation.JsonProperty;

import java.time.LocalDateTime;

public class AuthToken {

    /**
     * access_token: "3dbb796d-c225-403c-8c78-e16a51451452"
     * aid: "247f4d60-c14e-43ab-8506-ca6f19587a0a"
     * expires_in: 299
     * featureToggles: {boolean: {b1: true}, int: {i1: 1}, double: {d1: 5.45}, string: {s1: "string"},…}
     * gui_variant: "0"
     * is_push_otp_disable: false
     * last_login: "2020-10-11T14:42:00"
     * password_expires_in: "2021-02-28T18:39:19"
     * pbl: ["loan-calc-insuranceDelta", "new_design_for_history_ios", "loan_request_widget_android",…]
     * resource_owner: {branchId: 533, employerId: 3192599, categoryId: 85, segmentId: 4, firstName: "Максим",…}
     * sid: "b8e91f27-c07a-47ac-8611-c025889df7c1"
     * token_type: "bearer"
     * username: "mkumpan"
     */
    public final String accessToken;
    public final String aid;
    public final int expiresIn;
    public final String guiVariant;
    public final boolean isPushOtpDisable;
    public final LocalDateTime lastLogin;
    public final LocalDateTime passwordExpiresIn;
    public final String sid;
    public final String tokenType;
    public final String username;

    public AuthToken(@JsonProperty("access_token") String accessToken,
                     @JsonProperty("aid") String aid,
                     @JsonProperty("expires_in") int expiresIn,
                     @JsonProperty("gui_variant") String guiVariant,
                     @JsonProperty("is_push_otp_disable") boolean isPushOtpDisable,
                     @JsonProperty("last_login") LocalDateTime lastLogin,
                     @JsonProperty("password_expires_in") LocalDateTime passwordExpiresIn,
                     @JsonProperty("sid") String sid,
                     @JsonProperty("token_type") String tokenType,
                     @JsonProperty("username") String username) {
        this.accessToken = accessToken;
        this.aid = aid;
        this.expiresIn = expiresIn;
        this.guiVariant = guiVariant;
        this.isPushOtpDisable = isPushOtpDisable;
        this.lastLogin = lastLogin;
        this.passwordExpiresIn = passwordExpiresIn;
        this.sid = sid;
        this.tokenType = tokenType;
        this.username = username;
    }
}