package su.kumpan.raiffeisen;

import com.fasterxml.jackson.databind.ObjectMapper;
import su.kumpan.raiffeisen.model.AuthToken;
import su.kumpan.util.JsonBodyHandler;

import java.io.IOException;
import java.net.CookieManager;
import java.net.URI;
import java.net.http.HttpClient;
import java.net.http.HttpRequest;
import java.net.http.HttpResponse;
import java.util.HashMap;
import java.util.Map;
import java.util.function.Supplier;

public class OAuthConnector {

    private static final CookieManager COOKIE_HANDLER = new CookieManager();
    private final HttpClient client;

    public OAuthConnector() {
        client = HttpClient.newBuilder().cookieHandler(COOKIE_HANDLER).build();
    }

    public String getAuthKey(String username, String password) throws IOException, InterruptedException {
        Map<Object, Object> params = new HashMap<>();
        params.put("grant_type", "password");
        params.put("node", getNode());
        params.put("uiVersion", "RC3.0-GUI-5.26.2");
        params.put("password", password);
        params.put("username", username);
        params.put("reCaptchaResponse", null);

        var objectMapper = new ObjectMapper();
        String requestBody = objectMapper
                .writeValueAsString(params);

//        String encoding = "Basic " + Base64.getEncoder().encodeToString((username + ":" + password).getBytes());
        String encoding = "Basic b2F1dGhVc2VyOm9hdXRoUGFzc3dvcmQhQA==";

        HttpRequest request = HttpRequest.newBuilder()
                .POST(HttpRequest.BodyPublishers.ofString(requestBody))
                .uri(URI.create("https://online.raiffeisen.ru/oauth/token"))
                .setHeader("Authorization", encoding)
                .setHeader("Content-Type", "application/json;charset=UTF-8")
                .setHeader("Accept", "application/json, text/plain, */*")
                .build();
        HttpResponse<Supplier<AuthToken>> response = client.send(request, new JsonBodyHandler<>(AuthToken.class));

        return response.body().get().accessToken;
    }

    private String getNode() throws IOException, InterruptedException {
        HttpRequest request = HttpRequest.newBuilder(
                URI.create("https://online.raiffeisen.ru/rest/version"))
                .build();

        HttpResponse<String> response = client.send(request, HttpResponse.BodyHandlers.ofString());
        return response.body();
    }

    public static void main(String[] args) throws IOException, InterruptedException {
        System.out.println(new OAuthConnector().getAuthKey("mkumpan", "x8a?P2B#"));
    }
}
