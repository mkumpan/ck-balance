package su.kumpan.raiffeisen;

import org.apache.commons.io.FileUtils;
import su.kumpan.Account;

import java.io.File;
import java.io.IOException;
import java.net.URL;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.time.temporal.ChronoField;
import java.time.temporal.ChronoUnit;
import java.util.concurrent.TimeUnit;

public class BankStatementDownloader {

    public static final int DEFAULT_TIMEOUT = (int) TimeUnit.MINUTES.toMillis(1);

    DateTimeFormatter API_FORMAT = DateTimeFormatter.ofPattern("yyyy-MM-dd'T'HH:mm");
    DateTimeFormatter FILE_FORMAT = DateTimeFormatter.ofPattern("yyyyMMddHHmm");

    public File getStatement(Account account, LocalDateTime from, LocalDateTime to, String authKey) throws IOException {
        File destination = new File("./statements/statement_" + account + "_" + from.format(FILE_FORMAT) + "-" + to.format(FILE_FORMAT) + ".csv");
        FileUtils.copyURLToFile(
                new URL(buildUrl(account, authKey, from, to)),
                destination,
                DEFAULT_TIMEOUT,
                DEFAULT_TIMEOUT);

        return destination;
    }

    private String buildUrl(Account account, String authKey, LocalDateTime from, LocalDateTime to) {
        return "https://online.raiffeisen.ru/rest/"
                + (account.useCard() ? "card/" : "account/")
                + (account.useCard() ? account.getBankCardId() : account.getBankAcctId())
                + "/transaction.csv?"
                + buildFromClause(from)
                + buildToClause(to)
                + "&sort=date&order=desc"
                + "&access_token=" + authKey
                + "&download=";
    }

    private String buildFromClause(LocalDateTime from) {
        return "&from=" + from
                .with(ChronoField.NANO_OF_DAY, 0)
                .format(API_FORMAT);
    }

    private String buildToClause(LocalDateTime to) {
        return "&to=" + to.format(API_FORMAT);
    }
}
