package su.kumpan;

import java.time.LocalDate;
import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;

public class Issue implements DateContainer {
    private final List<Transaction> transactions;
    private final IssueType type;
    private final Set<LocalDate> dates;
    private final int index;

    public static int lastIndex = 0;

    public Issue(List<Transaction> transactions, IssueType type) {
        this.transactions = transactions;
        this.type = type;
        this.dates = transactions.stream().map(it -> it.getTime().toLocalDate()).collect(Collectors.toSet());
        this.index = lastIndex++;
    }

    public double sum() {
        double sum = 0;

        for (Transaction it : transactions) {
            sum += it.getAmount();
        }

        return sum;
    }

    public int getIndex() {
        return index;
    }

    public IssueType getType() {
        return type;
    }

    public List<Transaction> getTransactions() {
        return transactions;
    }

    public Set<LocalDate> getDates() {
        return dates;
    }

    @Override
    public String toString() {
        return type + " : " + transactions;
    }
}
