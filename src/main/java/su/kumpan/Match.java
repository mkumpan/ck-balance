package su.kumpan;

import com.google.common.base.Joiner;
import com.google.common.collect.ImmutableList;

import java.time.LocalDate;
import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;

public class Match implements DateContainer {
    private final List<Transaction> transactions;
    private final MatchType type;
    private final Set<LocalDate> dates;
    private final int index;

    public static int lastIndex = 0;

    public Match(Transaction left, Transaction right, MatchType type) {
        this.transactions = ImmutableList.of(left, right);
        this.type = type;
        this.dates = transactions.stream().map(it -> it.getTime().toLocalDate()).collect(Collectors.toSet());
        this.index = lastIndex++;
    }

    public Match(List<Transaction> transactions, MatchType type) {
        this.transactions = ImmutableList.copyOf(transactions);
        this.type = type;
        this.dates = transactions.stream().map(it -> it.getTime().toLocalDate()).collect(Collectors.toSet());
        this.index = lastIndex++;
    }

    public double sum() {
        double sum = 0;

        for (Transaction it : transactions) {
            sum += it.getAmount();
        }

        return sum;
    }

    public int getIndex() {
        return index;
    }

    @Override
    public Set<LocalDate> getDates() {
        return dates;
    }

    public MatchType getType() {
        return type;
    }

    public List<Transaction> getTransactions() {
        return transactions;
    }

    @Override
    public String toString() {
        return Joiner.on(" ").join(type, transactions);
    }
}
