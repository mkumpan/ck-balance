package su.kumpan;

import org.jetbrains.annotations.Nullable;

import java.util.*;

import static java.util.stream.Collectors.*;

public enum Account {
    PERSONAL("73701565", "7212819", "82b312fa-02b3-4efe-88c8-f3d560fd1513", false),
    COMMON("74240094", "7212818", "eff2ba274dab4b51b321b9094319ce97", false),
    CREDIT50("65998451", "16081796", "318331bfe9f34edb9cd4d10332f46541", true),
    CREDIT100("74202134", "18474090", "c22e4127-5fae-44aa-8df6-1aeceed451ce", true),
    DEPOSIT(null, "16081596", "0835a846-f563-4ac4-96c5-d42483b7b5fc", false),
    CASH_MAX("dfbe19b30f4d489f847c3c357bb43fa7"),
    CASH_ALINA("aff995de-fb54-4dba-86f1-3cf5847aa09c"),
    SALARY("50733a0d-f201-45bf-969b-e92db95787c6"),
    UNKNOWN(null, null, null, false);

    private final String bankCardId;
    private final String bankAcctId;
    private final String ckId;
    private final boolean useCard;

    private static Map<String, Account> byCkId = new HashMap<>();
    static {
        for (Account it : Account.values()) {
            if (it.getCkId() == null) continue;
            byCkId.put(it.getCkId(), it);
        }
    }

    Account(String bankCardId, String bankAcctId, String ckId, boolean useCard) {
        this.bankCardId = bankCardId;
        this.bankAcctId = bankAcctId;
        this.ckId = ckId;
        this.useCard = useCard;
    }

    Account(String ckId) {
        this(null, null, ckId, false);
    }

    public static Set<Account> bankAccounts() {
        return Arrays.stream(values()).filter(it -> it.getBankAcctId() != null).collect(toSet());
    }

    @Nullable
    public static Account byCkId(String destinationId) {
        return byCkId.getOrDefault(destinationId, UNKNOWN);
    }

    public String getBankCardId() {
        return bankCardId;
    }

    public String getCkId() {
        return ckId;
    }

    public String getBankAcctId() {
        return bankAcctId;
    }

    public boolean useCard() {
        return useCard;
    }
}
