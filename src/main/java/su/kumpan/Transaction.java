package su.kumpan;

import java.time.LocalDateTime;

public interface Transaction {
    Double getAmount();
    LocalDateTime getTime();
    String getDescription();
    TxType getType();

    enum TxType {
        CK, BANK
    }
}
