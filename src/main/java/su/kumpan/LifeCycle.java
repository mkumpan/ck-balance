package su.kumpan;

import su.kumpan.coinkeeper.CKCatReader;
import su.kumpan.coinkeeper.CKStatementDownloader;
import su.kumpan.raiffeisen.BankStatementDownloader;
import su.kumpan.raiffeisen.OAuthConnector;
import su.kumpan.raiffeisen.BankStatementParser;
import su.kumpan.reporting.CsvReporter;

import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.time.LocalDateTime;
import java.time.temporal.ChronoField;
import java.util.*;
import java.util.concurrent.TimeUnit;

import static java.time.temporal.ChronoUnit.MONTHS;

public class LifeCycle {

    public static void main(String[] args) throws IOException, InterruptedException {
        LocalDateTime from = LocalDateTime.now().minus(2, MONTHS).with(ChronoField.NANO_OF_DAY, 0);
        LocalDateTime to = LocalDateTime.now();

        Map<Account, DeltaReport> report = new LifeCycle().run(from, to);


        Map<String, String> mappings = new CKCatReader().retrieve(report.values());

        CsvReporter csvReporter = new CsvReporter(mappings);
        for (Account account : report.keySet()) {
            csvReporter.generate(account, report.get(account), from, to);
        }
    }

    public Map<Account, DeltaReport> run(LocalDateTime from, LocalDateTime to) throws IOException, InterruptedException {
        long start = System.nanoTime();

        Properties properties = new Properties();
        properties.load(new FileReader("src/main/resources/app.properties"));

        String authKey = new OAuthConnector().getAuthKey(properties.getProperty("username"), properties.getProperty("password"));

        BankStatementDownloader bankStatementDownloader = new BankStatementDownloader();
        DeltaCalculator calculator = new DeltaCalculator();

        Map<Account, DeltaReport> reports = new HashMap<>();

        for (Account account : Account.bankAccounts()) {
            System.out.println("Calculating for " + account + ", dated " + from + " to " + to);
            File bankStatement = null;
            try {
                bankStatement = bankStatementDownloader.getStatement(account, from, to, authKey);
            } catch (IOException e) {
                System.out.println("Failed to acquire statement for account " + account);
                continue;
            }

            Set<Transaction> bankTxs = new BankStatementParser().parse(bankStatement, account);
            Set<Transaction> ckTxs = new CKStatementDownloader(
                    properties.getProperty("ck.user"),
                    properties.getProperty("ck.auth")
            ).getStatement(account, from, to);

            bankStatement.deleteOnExit();
            System.out.println("Bank amounts total: " + sum(bankTxs));
            System.out.println("CK amounts total: " + sum(ckTxs));

            DeltaReport report = calculator.process(bankTxs, ckTxs);

            reports.put(account, report);
        }

        long end = System.nanoTime();


        System.out.println("Done, taken " + TimeUnit.NANOSECONDS.toSeconds(end - start) + " seconds");

        return reports;
    }

    private static double sum(Set<Transaction> txs) {
        double ttl = 0;
        for (Transaction tx : txs) {
            ttl += tx.getAmount();
        }

        return ttl;
    }
}
