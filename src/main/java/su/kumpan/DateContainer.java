package su.kumpan;

import java.time.LocalDate;
import java.util.Set;

public interface DateContainer {
    Set<LocalDate> getDates();
}
