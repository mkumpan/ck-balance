package su.kumpan;

import com.google.common.collect.HashMultimap;
import com.google.common.collect.ImmutableList;
import com.google.common.collect.ImmutableSet;
import com.google.common.collect.Multimap;

import java.time.LocalDate;
import java.time.temporal.ChronoUnit;
import java.util.Collection;
import java.util.Iterator;
import java.util.List;
import java.util.Set;
import java.util.concurrent.atomic.AtomicBoolean;
import java.util.function.Function;

import static su.kumpan.IssueType.IRREGULAR_DUPLICATES;
import static su.kumpan.MatchType.*;

public class DeltaCalculator {

    private static final double EPSILON = 1e-5;

    public DeltaReport process(Set<Transaction> bankTxs, Set<Transaction> ckTxs) {

        DeltaReport report = new DeltaReport(bankTxs, ckTxs);

        for (LocalDate day : report.getCoveredDays()) {

            int reportBankSize1 = report.getUnprocessedBankTxs().size();
            int reportCkSize1 = report.getUnprocessedCkTxs().size();

            int matchedExact = matchExactWithDuplicates(day, report);

            int reportBankSize2 = report.getUnprocessedBankTxs().size();
            int reportCkSize2 = report.getUnprocessedCkTxs().size();

            int matchedRough = matchRough(day, report);

            int a = 1;
        }

        matchOffByDay(report);
        failRemainder(report);

        return report;
    }

    private int matchRough(LocalDate day, DeltaReport report) {
        int count = 0;
        Set<Transaction> ckForDay = report.getCkForDay(day);
        Set<Transaction> bankForDay = report.getBankForDay(day);

        for (Transaction ckTx : ckForDay) {
            for (Transaction bankTx : bankForDay) {
                if (roughEquals(ckTx, bankTx)) {
                    report.match(ckTx, bankTx, ROUGH);
                    count++;
                }
            }
        }

        return count;
    }

    private void failRemainder(DeltaReport report) {
        report.getUnprocessedBankTxs().forEach(it -> report.registerIssue(IssueType.MISSING_IN_CK, ImmutableList.of(it)));
        report.getUnprocessedCkTxs().forEach(it -> report.registerIssue(IssueType.EXTRA_IN_CK, ImmutableList.of(it)));
    }

    private void matchOffByDay(DeltaReport report) {
        for (Transaction ckTx : report.getUnprocessedCkTxs()) {
            var found = lookTomorrow(ckTx, report);
            if (!found) lookYesterday(ckTx, report);
        }
    }

    private boolean lookTomorrow(Transaction tx, DeltaReport report) {
        LocalDate tomorrow = tx.getTime().toLocalDate().plus(1, ChronoUnit.DAYS);
        Set<Transaction> bankForTomorrow = report.getBankForDay(tomorrow);

        AtomicBoolean found = new AtomicBoolean(false);
        bankForTomorrow.forEach(it -> {
            if (equal(tx, it)) {
                report.match(tx, it, NEXT_DAY);
                found.compareAndSet(false, true);
            } else if (roughEquals(tx, it)) {
                report.match(tx, it, NEXT_DAY_ROUGH);
                found.compareAndSet(false, true);
            }
        });

        return found.get();
    }

    private void lookYesterday(Transaction tx, DeltaReport report) {
        LocalDate yesterday = tx.getTime().toLocalDate().minus(1, ChronoUnit.DAYS);
        Set<Transaction> bankForYesterday = report.getBankForDay(yesterday);

        bankForYesterday.forEach(it -> {
            if (equal(tx, it)) {
                report.match(tx, it, NEXT_DAY);
            }
            if (roughEquals(tx, it)) {
                report.match(tx, it, NEXT_DAY_ROUGH);
            }
        });
    }

    private boolean roughEquals(Transaction bankTx, Transaction ckTx) {
        return Math.abs(ckTx.getAmount() - bankTx.getAmount()) <= 1;
    }

    /**
     * Counts duplicate amounts per day. If they are unequal, they should all be reported.
     */
    private int matchExactWithDuplicates(LocalDate day, DeltaReport report) {
        int count = 0;
        Set<Transaction> bankForDay = report.getBankForDay(day);
        Set<Transaction> ckForDay = report.getCkForDay(day);

        Multimap<Double, Transaction> bankByAmt = toMultimapBy(bankForDay, it -> Math.floor(it.getAmount()));
        Multimap<Double, Transaction> ckByAmt = toMultimapBy(ckForDay, it -> Math.floor(it.getAmount()));

        ImmutableSet<Double> amounts = ImmutableSet.<Double>builder()
                .addAll(bankByAmt.keys())
                .addAll(ckByAmt.keys())
                .build();

        for (Double value : amounts) {
            Set<Transaction> bankTxs = report.getBankTxBy(day, value);
            Set<Transaction> ckTxs = report.getCkTxBy(day, value);
            if (ckTxs.size() == bankTxs.size()) {
                Iterator<Transaction> ckIterator = ckTxs.iterator();
                Iterator<Transaction> bankIterator = bankTxs.iterator();

                while (ckIterator.hasNext() && bankIterator.hasNext()) {
                    Transaction ckTx = ckIterator.next();
                    Transaction bankTx = bankIterator.next();

                    count++;
                    report.match(ckTx, bankTx, equal(ckTx, bankTx) ? EXACT : ROUGH);
                }
            } else {
                if ( !bankTxs.isEmpty() && !ckTxs.isEmpty()) {
                    count += bankTxs.size() + ckTxs.size();
                    report.registerIssue(IRREGULAR_DUPLICATES, ImmutableList.<Transaction>builder()
                            .addAll(ckTxs)
                            .addAll(bankTxs)
                            .build());
                }
            }
        }

        return count;
    }

    private boolean equal(Transaction left, Transaction right) {
        return Math.abs(left.getAmount() - right.getAmount()) < EPSILON;
    }

    private <T> Multimap<T, Transaction> toMultimapBy(Set<Transaction> transactions, Function<Transaction, T> keyExtractor) {
        HashMultimap<T, Transaction> result = HashMultimap.create();
        for (Transaction tx : transactions) {
            result.put(keyExtractor.apply(tx), tx);
        }

        return result;
    }
}
